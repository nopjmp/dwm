#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>

extern char *selfexe;

/**
 * Magically finds the current's executable path
 *
 * @return char* the path of the current executable
 */
char *get_dwm_path(){
	ssize_t r;
	char *path = NULL;

 	path = malloc(PATH_MAX);
        if (path == NULL){
		perror("malloc: get_dwm_path()");
		return NULL;
        }

        r = readlink("/proc/self/exe", path, PATH_MAX - 1);
        if (r < 0 || r > PATH_MAX) {
		perror("readlink: get_dwm_path()");
		return NULL;
        }

	path[r] = '\0';

	return path;
}

/**
 * self-restart
 *
 * Initially inspired by: Yu-Jie Lin
 * https://sites.google.com/site/yjlnotes/notes/dwm
 */
void self_restart(const Arg *arg) {
	char *const argv[] = {selfexe, NULL};

	if (argv[0] == NULL) {
		return;
	}

	execv(argv[0], argv);
}
